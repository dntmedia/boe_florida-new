# Template

Base for jump starting projects


## Official Documentation

Lumen: https://lumen.laravel.com/

This repositiory location: https://bitbucket.org/dntmedia/lumen-template


### Explantions

1. Lumen is a mini framework that can be converted to a full laravel install with a few minor changes.

2. To fire up an instance of this you will need to:

	a. edit the .env.example file and save it as .env
	b. point the vhost to the public directory

	the index.php file basically just registers all kinds of good stuff for the framework


3. Main files that will need to be edited:

app/Http/Controllers/PageController.php
	- when you add a new page it needs to be registered here. Basically all this controller does at the moment is to declare the page to be used
	- it can be extended to included database queries but a variable will need to be passed to the view

app/Http/routes.php
	- When you add a new page it will need to be added here too.
	The routes file allows clean urls and you can easily change a url by only editing this file. The controller will assign the view so that won't need to be changed.

4. Assets

	The assets are located in public/assets.

		assets/css/app.css 			--> baseline css for all sites
		assets/css/carousel.css 	--> at the moment just bootstrap's carousel css
		assets/css/site.css  		--> css that is specific to the project itself

		assets/images  				--> store all template images here

		assets/js/app.js 			--> intended to be a dump file for the js that is needed for the site

		assets/vendors 				--> this starter kit comes with bootstrap, fontawesome, jquery and my own css

	IF a project needs to have files uploaded by the user I'd recommend looking at a full laravel install


5. Resources / views = html

	All files that will need to be edit are esentially located here. The word "blade" needs to be part of the file for the templating engine to work.

	resources/views/_layouts/app.blade.php 			--> This is the main layout that calls all the other pieces, css and js
	resources/views/_partials/banner.blade.php 		--> This could be used instead of the carousel view
	resources/views/_partials/carousel.blade.php 	--> bootstrap's carousel html
	resources/views/_partials/content.blade.php 	--> Normally, you won't need to edit this but if you do the content that you add here will be sent to every page
	resources/views/_partials/footer.blade.php 		--> footer html
	resources/views/_partials/header.blade.php 		--> header html

	resources/views/pages/about.blade.php 		--> 
	resources/views/pages/home.blade.php 		--> consider this the indes page
	resources/views/pages/privacy.blade.php 	--> 
	resources/views/pages/template.blade.php 	--> I use this to as a template for all other pages but copy, paste and edit of another view works just as good
	resources/views/pages/terms.blade.php 		--> 
	resources/views/pages/welcome.blade.php 	--> DNT Media swank page. It's sort of an easter egg. It is registered in the routes.php file so if you don't want 													this popping up comment out that line in routes.php 
