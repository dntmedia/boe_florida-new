<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */

// $app->get('/', function () use ($app) {
//     return $app->version();
// });

// Basics ---------------------------------------------------------------------

$app->get('/', 'PageController@home');

// pages  ---------------------------------------------------------------------

$app->get('contact-us', 'PageController@contactUs');

// -- contact -----------------------------------------------------------------

$app->get('contact', 'ContactController@contact');
$app->post('send-contact', 'ContactController@postContact');

// about  ---------------------------------------------------------------------

$app->get('about/who-we-are', 'AboutController@whoWeAre');
$app->get('about/testimonials', 'AboutController@testimonials');
$app->get('about/our-team', 'AboutController@ourTeam');
$app->get('about/our-team/{id}', 'AboutController@getOfficer');
$app->get('about/partners', 'AboutController@partners');

// legal  ---------------------------------------------------------------------

$app->get('legal/analytics', 'LegalController@analytics');
$app->get('legal/counseling', 'LegalController@counseling');
$app->get('legal/identity_theft', 'LegalController@identitytheft');
$app->get('legal/patriot_act', 'LegalController@patriotact');
$app->get('legal/security', 'LegalController@security');

// products  ------------------------------------------------------------------

$app->get('products/bridge-loans', 'ProductsController@bridgeLoans');
$app->get('products/construction', 'ProductsController@construction');
$app->get('products/fha-loans', 'ProductsController@fhaLoans');
$app->get('products/jumbo-loans', 'ProductsController@jumboLoans');
$app->get('products/renovation', 'ProductsController@renovation');
$app->get('products/reverse-mortgage', 'ProductsController@reverseMortgage');
$app->get('products/usda-loans', 'ProductsController@usdaLoans');
$app->get('products/va-loans', 'ProductsController@vaLoans');
$app->get('products/vacation', 'ProductsController@vacation');

// tools  ---------------------------------------------------------------------

$app->get('tools/fastapp', 'ToolsController@fastapp');
$app->get('tools/mortgage-calculator', 'ToolsController@mortgageCalculator');
$app->get('tools/affordability', 'ToolsController@affordability');
$app->get('tools/rent-vs-buy', 'ToolsController@rentBuy');

$app->get('api/calculate-mortgage', 'ToolsController@calculateMortgage');
