<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * View : contact
     *
     * @return view
     */
    public function contact()
    {

        $extended_footer = "NO";

        return view('contact.contact',
            compact(
                'extended_footer'
            ));
    }

    /**
     * View : find-branch
     *
     * @return view
     */
    public function postContact(
        Request $request
    ) {

//         $uri = Request::path();
        // //        dd($uri);
        //         if ($uri == "send-contact") {
        //             $uri = "contact";
        //         }

        $from = env('CONTACT_FROM', '');
        $sendTo = env('CONTACT_TO', '');
        $subject = env('CONTACT_SUBJECT', 'New message from contact form');

        $fields = array('name' => 'Name', 'surname' => 'Lastname', 'phone' => 'Phone', 'email' => 'Email', 'message' => 'Message'); // array variable name => Text to appear in email
        $okMessage = 'Contact form successfully submitted. Thank you, a loan officer will be contacting you soon!';
        $errorMessage = 'There was an error while submitting the form. Please try again later';

// let's do the sending

        if (empty($_POST['address'])) {
            try
            {
                $emailText = "You have a new message from the BOEFlorida.com contact form.";

                foreach ($_POST as $key => $value) {

                    if (isset($fields[$key])) {
                        $emailText .= "$fields[$key]: $value\n";
                    }
                }

                mail($sendTo, $subject, $emailText, "From: " . $from);


                $responseArray = array('type' => 'success', 'message' => $okMessage);
            } catch (\Exception $e) {
                $responseArray = array('type' => 'danger', 'message' => $errorMessage);
            }

            // if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            //     $encoded = json_encode($responseArray);

            //     header('Content-Type: application/json');

            //     echo $encoded;
            // } else {
            //     echo $responseArray['message'];
            // }

            $message = $responseArray['message'];

        } // honeypot -- check for address being filled or not

        // return redirect()->route($uri);
        return view('contact.success',
            compact(
                'message'
            ));
    }

}
