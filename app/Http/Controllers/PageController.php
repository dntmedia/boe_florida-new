<?php

namespace App\Http\Controllers;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
//        Office $office
    ) {
//        $this->office = $office;
    }

    /**
     * View : welcome
     *
     * @return view
     */
    // public function welcome()
    // {
    //     return view('pages.welcome');
    // }

// Basics ---------------------------------------------------------------------

    /**
     * View : home
     *
     * @return view
     */
    public function home()
    {
        return view('pages.home');
    }

    /**
     * View : about
     *
     * @return view
     */
    // public function about()
    // {
    //     return view('pages.about');
    // }

    /**
     * View : terms and conditions
     *
     * @return view
     */
    // public function terms()
    // {
    //     return view('pages.terms');
    // }

    /**
     * View : privacy policy
     *
     * @return view
     */
    // public function privacy()
    // {
    //     return view('pages.privacy');
    // }

// Site Specific --------------------------------------------------------------

    /**
     * View : events
     *
     * @return view
     */
    public function events()
    {
        return view('pages.events');
    }

    /**
     * View : fastapp
     *
     * @return view
     */
    public function fastapp()
    {
        return view('pages.fastapp');
    }

        /**
     * View : blog
     *
     * @return view
     */
    public function blog()
    {
        return view('pages.blog');
    }

         /**
     * View : contact
     *
     * @return view
     */
    public function contactUs()
    {
        $extended_footer = false;
        return view('pages.contact-us',
            compact(
                'extended_footer'
            ));
    }

}
