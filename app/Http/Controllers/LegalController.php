<?php

namespace App\Http\Controllers;

class LegalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * View : analytics
     *
     * @return view
     */
    public function analytics()
    {
        return view('legal.analytics');
    }

    /**
     * View : counseling
     *
     * @return view
     */
    public function counseling()
    {
        return view('legal.counseling');
    }

    /**
     * View : identitytheft
     *
     * @return view
     */
    public function identitytheft()
    {
        return view('legal.identitytheft');
    }

    /**
     * View : patriotact
     *
     * @return view
     */
    public function patriotact()
    {
        return view('legal.patriotact');
    }

    /**
     * View : security
     *
     * @return view
     */
    public function security()
    {
        return view('legal.security');
    }

}
