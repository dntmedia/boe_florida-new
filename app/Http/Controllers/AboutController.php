<?php

namespace App\Http\Controllers;

use DB;

class AboutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

// View -----------------------------------------------------------------------
    /**`
     * View : getOfficer
     *
     * @return view
     */
    public function getOfficer($id)
    {
        $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_member&id=" . $id;
        $json = file_get_contents($pathOrUrl);
        $content = json_decode($json, true);

        $first_name = $content[0]['fname'];
        $last_name = $content[0]['lname'];
        $job_title = $content[0]['title'];
        $nmls = $content[0]['nmls'];
        $office = $content[0]['phone'];
        $mobile = $content[0]['mobile'];
        $fax = $content[0]['fax'];
        $email = $content[0]['email'];
        $apply_link = 'https://3146532938.mortgage-application.net/WebApp/Start.aspx';
        $biography = $content[0]['biography'];
        $photo = $content[0]['photo'];

        $employee_info = DB::select('SELECT * FROM employee_extra where employee_id=:id and active=:active', ['id' => $id, 'active' => 1]);

        return view('about.officer',
            compact(
                'first_name', 
                'last_name',
                'job_title',
                'nmls',
                'office',
                'mobile',
                'fax',
                'email',
                'apply_link',
                'photo',
                'biography',
                'employee_info'
            ));
    }
    /**
     * View : our-team
     *
     * @return view
     */
    public function ourTeam()
    {
        $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_team&branch=" . env('BRANCH_ID');
        $json = file_get_contents($pathOrUrl);
        $officers = json_decode($json, true);
        return view('about.our-team',
            compact(
                'officers'
            ));

    }

    /**`
     * View : testimonials
     *
     * @return view
     */
    public function testimonials()
    {
        $picRandom = rand(1, 7);

        return view('about.testimonials',
            compact(
                'picRandom'
            ));
    }
    /**
     * View : top-ten-reasons
     *
     * @return view
     */
    public function partners()
    {
        return view('about.partners');
    }

    /**
     * View : who-we-are
     *
     * @return view
     */
    public function whoWeAre()
    {
        return view('about.who-we-are');
    }

}
