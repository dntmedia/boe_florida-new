@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Our Team @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')



<section id="about" style="margin-top: 95px">
    <img src="{{ url('images/home/florida_team.jpg') }}" alt="Bank of England Mortgage Florida" class="img-responsive">
    <div class="container officer-content">
      <h1 class="text-center" style="margin-bottom: 40px">BOE Sarasota Team</h1>
        <div class="row">
          @foreach($officers as $officer) 
          @if ($officer['title'] == "Area Manager")
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="thumbnail team-thumbnail">
            <a href="/about/our-team/{{ $officer['id'] }}">
              <img src="{{ $officer['photo']  }}" style="border: 1px solid black" alt="{{ $officer['fname'] }} {{ $officer['lname'] }}">
            </a>
            <div class="caption about-caption">
              <h3>{{ $officer['fname'] }} {{ $officer['lname'] }}</h3>
              <h4>{{ $officer['title'] }}</h4>
              <p>NMLS # {{ $officer['nmls'] }}</p>
              <p>Office: {{ $officer['phone'] }}</p>
              <p>Mobile: {{ $officer['mobile'] }}</p>
              <p>Fax: {{ $officer['fax'] }}</p>
              <p><a href="{{ $officer['email'] }}">{{ $officer['email'] }}</a></p>
              <p>
                <a href="/about/our-team/{{ $officer['id'] }}" class="btn main-btn">Biography</a>
                <a href="https://3146532938.mortgage-application.net/WebApp/Start.aspx" class="btn main-btn">Apply</a>
              </p>

            </div>
            </div>
          </div>
          @endif
          @endforeach

          @foreach($officers as $officer) 
          @if ($officer['title'] == "Sales Manager" || $officer['title'] =="Processing Manager")
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="thumbnail team-thumbnail">
            <a href="/about/our-team/{{ $officer['id'] }}">
              <img src="{{ $officer['photo']  }}" style="border: 1px solid black" alt="{{ $officer['fname'] }} {{ $officer['lname'] }}">
            </a>
            <div class="caption about-caption">
              <h3>{{ $officer['fname'] }} {{ $officer['lname'] }}</h3>
              <h4>{{ $officer['title'] }}</h4>
              <p>NMLS # {{ $officer['nmls'] }}</p>
              <p>Office: {{ $officer['phone'] }}</p>
              <p>Mobile: {{ $officer['mobile'] }}</p>
              <p>Fax: {{ $officer['fax'] }}</p>
              <p><a href="{{ $officer['email'] }}">{{ $officer['email'] }}</a></p>
              <p>
                <a href="/about/our-team/{{ $officer['id'] }}" class="btn main-btn">Biography</a>
                <a href="https://3146532938.mortgage-application.net/WebApp/Start.aspx" class="btn main-btn">Apply</a>
              </p>

            </div>
            </div>
          </div>
          @endif
          @endforeach

          @foreach($officers as $officer) 
          @if ($officer['title'] == "Mortgage Banker" || $officer['title'] == "Senior Mortgage Banker")
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="thumbnail team-thumbnail">
            <a href="/about/our-team/{{ $officer['id'] }}">
              <img src="{{ $officer['photo']  }}" style="border: 1px solid black" alt="{{ $officer['fname'] }} {{ $officer['lname'] }}">
            </a>
            <div class="caption about-caption">
              <h3>{{ $officer['fname'] }} {{ $officer['lname'] }}</h3>
              <h4>{{ $officer['title'] }}</h4>
              <p>NMLS # {{ $officer['nmls'] }}</p>
              <p>Office: {{ $officer['phone'] }}</p>
              <p>Mobile: {{ $officer['mobile'] }}</p>
              <p>Fax: {{ $officer['fax'] }}</p>
              <p><a href="{{ $officer['email'] }}">{{ $officer['email'] }}</a></p>
              <p>
                <a href="/about/our-team/{{ $officer['id'] }}" class="btn main-btn">Biography</a>
                <a href="https://3146532938.mortgage-application.net/WebApp/Start.aspx" class="btn main-btn">Apply</a>
              </p>

            </div>
            </div>
          </div>
          @endif
          @endforeach

          @foreach($officers as $officer) 
          @if ($officer['title'] != "Mortgage Banker" && $officer['title'] != "Senior Mortgage Banker" && $officer['title'] != "Area Manager" && $officer['title'] != "Sales Manager" && $officer['title'] != "Processing Manager")
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="thumbnail team-thumbnail">
            <a href="/about/our-team/{{ $officer['id'] }}">
              <img src="{{ $officer['photo']  }}" style="border: 1px solid black" alt="{{ $officer['fname'] }} {{ $officer['lname'] }}">
            </a>
            <div class="caption about-caption">
              <h3>{{ $officer['fname'] }} {{ $officer['lname'] }}</h3>
              <h4>{{ $officer['title'] }}</h4>
              <p>NMLS # {{ $officer['nmls'] }}</p>
              <p>Office: {{ $officer['phone'] }}</p>
              <p>Mobile: {{ $officer['mobile'] }}</p>
              <p>Fax: {{ $officer['fax'] }}</p>
              <p><a href="{{ $officer['email'] }}">{{ $officer['email'] }}</a></p>
              <p>
                <a href="/about/our-team/{{ $officer['id'] }}" class="btn main-btn">Biography</a>
                <a href="https://3146532938.mortgage-application.net/WebApp/Start.aspx" class="btn main-btn">Apply</a>
              </p>

            </div>
            </div>
          </div>
          @endif
          @endforeach
        </div><!-- end row -->
    </div><!-- end container -->
    
</section>

@stop
