@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Who We Are @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')



<section id="about" style="margin-top: 95px">
    <img src="{{ url('images/home/sarasota_brewedlife.jpg') }}" alt="Bank of England Mortgage Sarasota attending Brewed Life" class="img-responsive">
    <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="who-we-are-title">
          <h1>About BOE Mortgage</h1>
        </div>
        <div class="who-we-are-body">
          <p>At Bank of England Mortgage, we take pride in being there when our borrowers need us, day and night. Our mission is to deliver total value to our customers. This means competitive rates, STRESS-FREE closings, extensive product knowledge, and a friendly helpful attitude.</p>
          <p>Bank of England Mortgage offers the strengths of a strong capital base, a solid and committed business plan and experienced management along with the assurance of a successful track record.</p>
          <p>Since our doors opened in 1898 in England, Arkansas, The Bank of England has been providing down home service. Along with powerful nationwide mortgage loans, we offer clients peace of mind knowing they are working with a credible national mortgage banker. Our success is due primarily to the talent of our people and access to hundreds of mortgage products at industry best pricing. Our management team has a combined 150 years of mortgage experience and is actively involved as members and board members on local, state and national mortgage broker and banker associations.</p>
          <p>Building on that foundation, we have grown to a company of more than 1000 employees with branches in over 39 states. We have 93 locations nationwide to serve your mortgage needs.</p>
          <p>We are the only mortgage lender in Southwest Florida that offers a true in-house processing and underwriting method. This sets us apart from all of our competition because it all allows us to keep complete control of every deal with faster closings. Navigating today’s mortgage market can be very challenging so having an entire team of professionals in your own backyard really makes a huge difference. We provides a full array of mortgage products including Conventional Loans, FHA loans, VA Loans, JUMBO Loans, Bridge loans and Condo Loans. Our ability to lend nationwide is an advantage, especially for those looking to purchase a second home in our great state of Florida (or anywhere else in the US).  For those who have homes in other states we can still originate, process and underwrite right out of our downtown Sarasota office. All of our employees including the loan officers, processors, underwriters and managers live, work and play in Southwest Florida. Unlike most mortgage companies or banks most of our staff has been with us for many years. This means that all of the professionals working on your loan have a unique understanding of the specific  home, townhome or condo you may be purchasing or refinancing.</p>
          <p>Our Sarasota team has doubled in the last few years, which in turn creates more jobs for people in our local community. We are very excited to share in your home financing journey, we look forward to exceeding your expectations.  BOE Mortgage provides a mortgage service unlike any other and we look forward to continued growth and positive influence in our much loved community.</p>
          <div id="about_sarasota"></div> 
          <hr>
      </div>
  </div>
  <div class="col-md-12" style="margin-top: 50px">
        <div class="who-we-are-subtitle">
        <img src="{{ url('images/home/slider_2.jpg') }}" alt="BOE Mortgage Florida area" class="img-responsive">
          <h2>About Sarasota, FL</h2>
        </div>
        <div class="who-we-are-body">
          <p>The beauty of Sarasota will get your attention on your first visit. The colors of the Gulf of Mexico and Sarasota Bay, contrasting with the brilliant white sand will make you anxious to see more. Sarasota boasts some of the oldest and newest homes in the area. Some homes were built over 50 years ago as vacation cottages, while others are new gated bay front communities. Residents from any neighborhood can enjoy a wide range of activities available throughout Sarasota. Whether your taste is in art, music, educational opportunities, sports, boating, or state parks, there is something for everyone. The four legged family members will fit right in too. Sarasota provides “paw parks” for dogs, where you can have a safe and pleasant experience with your pooch. Sarasota offers a wide assortment of celebration. Whether it’s about a life revolving around the great outdoors, or immersing yourself in a culturally rich society, there is plenty to do for you, the retiree, and the whole family.</p>
          
          <div class="row">
            <div class="col-sm-6">
              <h3>Top 10 Reasons to live in Sarasota</h3>
              <ol>
                <li>Residents benefit from no state income tax, low property taxes, and a variety of budget-friendly housing options, and low energy costs.</li>
                <li>No matter what cultural and arts activities you enjoy, Sarasota has something for you year round.</li>
                <li>Greater Sarasota offers a vast array of housing options- from villas to townhomes, to condominiums, to historic houses, to mansions, to large acreage estates.</li>
                <li>They have four local hospital facilities that provide exceptional healthcare services throughout the region.</li>
                <li>Sarasota is home to more than fourteen hundred restaurants from elegant eateries to fantastic food trucks.</li>
                <li>There are a variety of educational options for children and adults from traditional public schools, to charter schools, private and Montessori settings, adult education facilities, as well as universities.</li>
                <li>You can enjoy native wildlife at one of the 120 parks and trails, encompassing over 3000 acres.</li>
                <li>Sarasota is home to four tropical barrier islands.</li>
                <li>The average year round temperature is around 74 degrees.</li>
                <li>You can shop till you drop! Sarasota has three major shopping malls, from simple to extravagant.</li>
              </ol>
            </div>
            <div class="col-sm-6">
              <h3>For More Information</h3>
              <ul>
                <li>
                  <a href="http://www.sarasotagov.org/#3" target="_blank">http://www.sarasotagov.org/#3 - City of Sarasota</a>
                </li>
                <li>
                  <a href="http://www.schooldigger.com/go/FL/district/01680/search.aspx" target="_blank">http://www.schooldigger.com/go/FL/district/01680/search.aspx - Schools</a>
                </li>
                <li>
                  <a href="https://sarasotamagazine.com/2012/04/01/the-next-hot-neighborhoods/" target="_blank">https://sarasotamagazine.com/2012/04/01/the-next-hot-neighborhoods/ - Neighborhoods</a>
                </li>
                <li>
                  <a href="http://www.visitsarasota.org/events#/34236-sarasota/all/today/" target="_blank">http://www.visitsarasota.org/events#/34236-sarasota/all/today/ - Events</a>
                </li>
                <li>
                  <a href="http://sarasotamanateerealtors.com/education/calendar/" target="_blank">http://sarasotamanateerealtors.com/education/calendar/ - Calendar</a>
                </li>
              </ul>
            </div>
          </div>
      </div>
  </div>
</section>

@stop
