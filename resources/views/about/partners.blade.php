@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Our Partners @parent 
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')


        <div style="margin-top: 95px;">
        </div>
        <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="about-title">
              <h1>
				        BOE Mortgage Partners
              </h1>
              <p class="lead text-center" style="margin-bottom: 40px">BOE Mortgage supports and promotes local business and entrepreneurs.</p>
            </div>
            <div class="about-body">
              
              <div class="containers">
                <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail">
                      <a href="http://www.finepropertiesfl.com/" target="_blank"><img src="{{ url('images/about/partners/remax-500.png') }}" alt="RE/MAX Fine Properties"></a>
                      <div class="caption partner-title">
                        <p>RE/MAX Fine Properties</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail">
                      <a href="http://www.kwotw.com/" target="_blank"><img src="{{ url('images/about/partners/KellerWilliams_OnTheWaterSarasota_Logo_CMYK.png') }}" alt="Keller Williams On the Water"></a>
                      <div class="caption partner-title">
                        <p>Keller Williams On the Water</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail">
                      <a href="http://www.homesandland.com/For-Sale/Sarasota,FL/" target="_blank"><img src="{{ url('images/about/partners/Homes-&-Land-Logo-Blue.png') }}" alt="Homes and Land Magazine of Sarasota"></a>
                      <div class="caption partner-title">
                        <p>Homes and Land Magazine of Sarasota</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail">
                      <a href="http://sarasotamanateerealtors.com/" target="_blank"><img src="{{ url('images/about/partners/realtor-association.jpg') }}" alt="Realtor Association of Sarasota and Manatee"></a>
                      <div class="caption partner-title">
                        <p>Realtor Association of Sarasota and Manatee</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail">
                      <a href="http://getrealexclusive.com/" target="_blank"><img src="{{ url('images/about/partners/get-REAL-exclusive-logo-black.png') }}" alt="REAL Exclusive"></a>
                      <div class="caption partner-title">
                        <p>Real Exclusive</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail">
                      <a href="http://www.sarasotachamber.com/" target="_blank"><img src="{{ url('images/about/partners/Sarasota-Chamber-Membership.jpg') }}" alt="Sarasota Chamber of Commerce"></a>
                      <div class="caption partner-title">
                        <p>Sarasota Chamber of Commerce</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail">
                      <a href="http://www.yourobserver.com/lists/longboat-key/real-estate/all" target="_blank"><img src="{{ url('images/about/partners/Longboat_Observer.png') }}" alt="Longboat Observer"></a>
                      <div class="caption partner-title">
                        <p>Longboat Observer</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail">
                      <a href="http://www.yourobserver.com/lists/east-county/real-estate/all" target="_blank"><img src="{{ url('images/about/partners/East_County_Observer.png') }}" alt="East County Observer"></a>
                      <div class="caption partner-title">
                        <p>East County Observer</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail">
                      <a href="http://www.yourobserver.com/lists/sarasota/real-estate/all" target="_blank"><img src="{{ url('images/about/partners/Sarasota_Observer.png') }}" alt="Sarasota Observer"></a>
                      <div class="caption partner-title">
                        <p>Sarasota Observer</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="thumbnail">
                      <a href="http://www.yourobserver.com/lists/sarasota/real-estate/all" target="_blank"><img src="{{ url('images/about/partners/Siesta_Key_Observer.png') }}" alt="Siesta Key Observer"></a>
                      <div class="caption partner-title">
                        <p>Siesta Key Observer</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>


@stop
