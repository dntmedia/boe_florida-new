@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Testimonials @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')


        <div style="margin-top: 95px;">
          <img src="{{ url('images/testimonial_random/'.$picRandom.'.jpg') }}" alt="What a few of our customers had to say" class="img-responsive">
        </div>
        <div class="container testimonial-body">
        <div class="row">
          <div class="col-md-12">
            <h3>Here is what a few of our customers had to say...</h3>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="10000">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <blockquote>
                    <p>It was a pleasure from the beginning to the end. My family is very thankful for all you did in helping us. The process was about as smooth as could be and that was mainly due to your prompt attention to the details. I was very satisfied with your help and will store your number and highly recommend you to family and friends. </p>
                    <p>Thanks again and many blessings to you and your families</p>
                    <cite title="Source Title">- De La Rosa Family </cite>
                  </blockquote>
          </div>
          <div class="item">
            <blockquote>
                    <p>Bank of England was the only bank that helped me refinance my house. As a retired Lt. Col. with combat time in Desert Storm I am very grateful. For what Mr. Meshberger and Mr. Dogan did for me I thank you. God Bless the Bank of England.</p>
                    <cite title="Source Title">- Mike Hall, Lt Col USAF (ret)</cite>
                  </blockquote>
          </div>
          <div class="item">
            <blockquote>
                    <p>Bank of England Mortgage rocks... Professional-Brilliant-Organized-Kind-Caring and Willing to Go the Distance to assist with mortgage needs. (Small or Large)Thank you from the bottom of our hearts for ALL YOUR HELP!</p>
                    <p>When we were recommended to Bank of England Mortgage...we were on our 2nd Mortgage Company ! OMG "We were so frustrated with the mortgage process...it seemed almost impossible!" The previous mortgage companies...DID NOT EXPLAIN any details of the process , entered all of our data wrong and so on----As the owners of a small business, we were just about to give up!</p>
                    <p>Then the sellers recommended Bank of England Mortgage. We called and e mailed BOE and... WE GOT A CALL AND E MAIL BACK FROM ODAY ON THE WEEKEND IMMEDIATELY!</p>
                    <p>AND...THAT IS THE BEGINNING OF BOE becoming our HEROS...!!!</p>
                    <p>They returned calls and e mails QUICKLY with complete CLEAR explanations to assist us in obtaining a loan. Oday....met with us in his office and ACTUALLY PERSONALLY walked us through the steps on his Computer to help us visualize ways to tackle difficult hurdles with our mortgage. "Oday and BOE is a CAN DO TEAM!" "Oday made US FEEL LIKE WE WERE HIS ONLY CLIENT!"....."This is a COMPANY THAT CARES...regardless of the type or size of mortgage!" "Oday passed us on to Jennifer/Senior Loan Processor for BOE.....WOW...."When Jennifer would e mail...she always closed with Team Pohl!" "It made us feel so special...!!!" Jennifer would carefully layout clear concise lists of steps for us to handle to complete the loan process. (Always with a positive note...You Can Do It!) Jennifer found so many mistakes that the other mortgage companies had made on our file....Including wrong Socials, Split Files, Credit Errors....and The Lists goes on and on...! Oday and Jennifer would work with the Owner, Brokers to make sure all were well informed and clear on loan status!!! AND....WHEN THE APPROVAL CAME...THEY CELEBRATED WITH US!!!</p>
                    <p>From Beginning to End...BOE IS AMAZING AT THE MORTGAGE PROCESS AND ARE A BLESSING TO THOSE THAT USE THEIR SERVICES! We are so VERY GRATEFUL FOR BOE! "IN THIS BIG CRAZY WORLD...BOE'S HEART IS IN THE RIGHT PLACE!"</p>
                    <cite title="Source Title">- Craig and Fredda Pohl </cite>
                  </blockquote>
          </div>
        </div>
        </div>
      </div>

@stop
