@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
{{ $first_name }} {{ $last_name }} @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')



<section id="about" style="margin-top: 95px">
    <img class="img-responsive img-border-bottom" src="{{ url('images/employee/bio/' . $employee_info[0]->image_personal) }}" alt="Bank of England Mortgage Florida, {{ $first_name }} {{ $last_name }}">
    <div class="container officer-content">
      <div class="row">
        <div class="col-md-3">
          <div class="thumbnail team-thumbnail">
              <img src="{{ $photo }}" class="img-responsive" style="border: 1px solid black" alt="{{ $first_name }} {{ $last_name }}">
              <div class="caption about-caption">
                <h4>{{ $job_title }}</h4>
                <p>NMLS# {{ $nmls }}</p>
                <p>Office: {{ $office }} </p>
                <p>Mobile: {{ $mobile }}</p>
                <p>Fax: {{ $fax }} </p>
                <p><a href="mailto:{{ $email }}">{{ $email }}</a></p>
                <p>
                  <a href="{{ $apply_link }}" target="_blank" class="btn main-btn">Apply Now</a>
                </p>
              </div>
            </div>
        </div>
        <div class="col-md-9">
          <h2>Meet {{ $first_name }} {{ $last_name }}</h2>
          {!! $biography !!}
        </div>
      </div><!-- end row -->
      <div style="margin: 20px 0">
        <a href="/about/our-team" class="btn main-btn"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Back to your mortgage team</a>
      </div>
      
    </div><!-- end container -->

</section>

@stop
