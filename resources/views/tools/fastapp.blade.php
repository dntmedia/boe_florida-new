@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
BOE Florida FastApp @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')

  <section id="first-section" style="position: relative;">
    <div>
      <img src="{{ url('images/~images/green_background3.jpg') }}" class="img-responsive" alt="Bank of England Mortgage FastApp">
    </div>
    <div class="fastapp_download_logos fastapp-green">
        <div>
          <a href="https://itunes.apple.com/us/app/boe-florida-fastapp/id1167074961" target="_blank"><img src="{{ url('images/~images/appstore.png') }}" alt="Download FastApp from the App Store" class="img-responsive"></a>
        </div>
        <div>
          <a href="https://play.google.com/store/apps/details?id=com.boefastapp.florida" target="_blank"><img src="{{ url('images/~images/googleplay.png') }}" alt="Download FastApp from the Google Play store" class="img-responsive"></a>
        </div>
    </div>
  </section>
  <section class="fastapp-section">
    <div class="container">
      <div class="fastapp-description">
        <p><span>The Bank of England Mortgage FastApp</span> is the first of its generation.  We built it to streamline the home mortgage process and it's available today.  Click on the download links above based on your mobile device and get your <span>FastApp</span> today!</p>
      </div>
    <div class="row fastapp">
        <h3 class="fastapp-heading">What It Offers A Borrower:</h3>
        <div class="col-md-12">
          <div class="col-md-6">
            <ul>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Ability to apply quickly in a convenient interview style</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Superior Communication tool with direct messaging service</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Real-Time Loan Status Updates via Push Notifications</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Loan Status Bar</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Loan Information</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Ability to upload loan documents that saves to the eFolder within Encompass</li>
            </ul>
          </div>
          <div class="col-md-6">
            <ul>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Request on the fly pre-qualification letter</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Contact information from the branch, loan officer and realtor</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Title Company Location and Map Service</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Company Information</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Branch Location Service</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Loan Tools and Calculator</li>
            </ul>
          </div>
        </div>
      </div>
       <div class="row fastapp fastapp-last">
        <h3 class="fastapp-heading">What It Offers A Realtor:</h3>
        <div class="col-md-12">
          <div class="col-md-6">
            <ul>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Superior Communication tool with direct messaging service</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Real-Time Loan Status Updates via Push Notifications</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Loan Status Bar</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Loan Information</li>
            </ul>
          </div>
          <div class="col-md-6">
            <ul>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Contact information from the branch, loan officer and realtor</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Title Company Location and Map Service</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Branch Location Service</li>
              <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Loan Tools and Calculator</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>

@stop
