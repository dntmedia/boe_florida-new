@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Mortgage Calculator @parent
@stop

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/sweetalert/sweetalert.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/alert.css') }}">
@stop

@section('scripts')
	<script type="text/javascript" src="{{ url('assets/vendors/sweetalert/sweetalert.min.js') }}"></script>
@stop

@section('inline-scripts')
function loancalc() {
    buyingCosts = []; // new Array();
    rentingCosts = []; // new Array();

	var homePrice=document.getElementById('homePrice').value;
	var monthlyRent=document.getElementById("monthlyRent").value;
    var whichYear = 99;

    if (homePrice === 'undefined') {homePrice = 100000;} // default value
    else {homePrice = parseFloat(homePrice.toString());}

    if (monthlyRent === 'undefined') {monthlyRent = 800;} // default value
    else {monthlyRent = parseFloat(monthlyRent.toString());}

    // Net costs compare the total amount of money you would be spending over time,
    // minus the potential value you might receive if you someday sell the property.

    var buyingClosingCosts = 4000;            // this is the amount it cost to close a lon for $100k
    var buyingDownPaymentPercent = 0.20       // this is a 20% initial down payment
    var buyingCostsPerYear = 0.0711;           // this is the cost to buy for each year (% of purchase price)

    var houseAppreciation = 0.02;             // this is how much your house gains in value as the years go by.
    var sellingClosingCostPercent = 0.06;     // this is the % you py for closing costs.

    // The costs you pay at closing when you buy a home. This includes the down payment and buyer's closing costs.
    var buyingIntialCosts = parseFloat((homePrice * buyingDownPaymentPercent) + (homePrice / 100000 * buyingClosingCosts));

    // the priciple raminaing is equal to the house price less your down payment.
    var housePrincipleRemaing = parseFloat(homePrice - buyingIntialCosts + buyingClosingCosts);
    // console.log("housePrincipleRemaing: " + housePrincipleRemaing);

    var priciplePaidPerYearPercent = 0.01435;
    var houseValue = parseFloat(homePrice); // set the intitial value equal to the intial price.
    var buyingYearlyCost = 0;
    var ttlCostForBuyingHome = 0;

    // Renting
    var rentingIntialCosts = monthlyRent;       // a deposit of first month's rent
    var monthlyRentersInsurancePercent = 0.0132;
    var rentingYearlyCost = 0;
    var currentRent = monthlyRent;
    var yearlyRentAppreciation = 0.02;          // the amount the rent goes up yearly

    for (i = 0; i < 30; i++) {
      // loop for 30 years and build to arrays
      // console.log("******* START OF YEAR " + (i+1) + "*******");
      //console.log("buyingIntialCosts: " + buyingIntialCosts);

      // Buying - Yearly costs are recurring monthly or yearly expenses.
      buyingYearlyCost += parseFloat(homePrice * buyingCostsPerYear);
      //console.log("buyingYearlyCost: " + buyingYearlyCost);
      buyingCostsPerYear += 0.00175;           // this goes up slightly each year based on inflation and/or other incurred costs

      housePrincipleRemaing -= parseFloat(homePrice * priciplePaidPerYearPercent);
      if (housePrincipleRemaing < 0) { housePrincipleRemaing = 0; }
      //console.log("housePrincipleRemaing: " + housePrincipleRemaing);

      priciplePaidPerYearPercent += 0.0038; // to simulate paying more principle each year

      // calculate the current value of the house
      houseValue += parseFloat(houseValue * houseAppreciation);
      //console.log("houseValue: " + houseValue);

      // selling costs -- these are the cost incurred to sell your house after this many yeras.
      var sellingClosingCost = parseFloat(houseValue * sellingClosingCostPercent);
      var ttlSellingCost = parseFloat(sellingClosingCost + housePrincipleRemaing);
      //console.log("ttlSellingCost: " + ttlSellingCost);

      var ttlCostForBuyingHome = parseFloat(buyingIntialCosts + buyingYearlyCost + ttlSellingCost - houseValue); // subtract money gained from house sale.
      //console.log("Cost For Buying A Home: " + ttlCostForBuyingHome);

      var ttlMonths = (12 * (i + 1) );
      //console.log("Total Months: " + ttlMonths);

      var buyingMonthlyCosts = parseFloat(ttlCostForBuyingHome / ttlMonths);
      // console.log("buyingMonthlyCosts: " + buyingMonthlyCosts );               // <== target value #1

      var rentersInsurance = monthlyRent * monthlyRentersInsurancePercent;

      rentingYearlyCost += parseFloat((rentersInsurance + currentRent) * 12);

      currentRent += (currentRent * yearlyRentAppreciation); // increases after the year is over

      var ttlCostOfRenting = parseFloat(rentingYearlyCost - monthlyRent); // return depoist

      var rentingMonthlyCosts = parseFloat(ttlCostOfRenting / ttlMonths);

      // the target values will need to be saved so they can be displayed.
      buyingCosts.push(buyingMonthlyCosts.toFixed(2));
      rentingCosts.push(rentingMonthlyCosts.toFixed(2));

      if (whichYear == 99) {
        // if the threshold year has not been found yet, compare the two target values
        if (buyingMonthlyCosts <= rentingMonthlyCosts) {whichYear = i + 1;}
        	}
    	}
	//console.log(buyingCosts);

	var title="Buying will become cheaper than renting in <strong>" + whichYear + " years</strong>.";

	var output = '';
//	var output="<hr size=\"1\"><p style=\"margin-top:24px;\">Buying will become cheaper than renting in <strong>" + whichYear + " years</strong>.</p>";
	output += "<table border=\"0\" style=\"width:100%;\"><tr><th style=\"text-align:center;\">Year</th><th style=\"text-align:center;\">Avg. Monthly Buying Cost</th><th style=\"text-align:center;\" style=\"text-align:center;\">Avg. Monthly Rental Cost</th></tr>";
	for (i = 1; i < 30; i++) {
		output += "<tr><td width='20%'>" + i + "</td><td align=\"center\" width='40%'>$" + buyingCosts[i] + "</td><td align=\"center\" width='40%'>$" + rentingCosts[i] + "</td></tr>";
		}
	output += "</table>";


//	document.getElementById("amount").innerHTML = output;
	sweetAlert({
		html:true,
//		type: "info",
		customClass: 'swal-wide',
		title: title,
		text:output
		});

}
@stop


{{-- Content --}}
@section('content')
<div class="container">

    <section id="">
      <div class="container">
        <div class="row body-margin-top">
			<h1 class="text-center">Renting vs. Buying Calculator</h1>

<br>

<h4>
Should you buy or rent? This is a question most of us will likely face in our lives, whether buying a house makes more financial sense than renting a home. There is a way to understand the financial impact of buying vs. renting. This rent vs. buy calculator can help you calculate the net cost of buying a home versus the cost of renting over time.
</h4>

<br>

			<form>

				<p>
					<strong>Desired Home Price</strong>
					<br />
					<input type="text" id="homePrice" value="100000" style="font-size:18px;padding:5px;" />
				</p>

				<p>
					<strong>Desired Monthly Rent</strong>
					<br />
					<input type="text" id="monthlyRent" min='0' max='12' value="800" style="font-size:18px;padding:5px;" />
				</p>

				<p>
					<input type="button" value="Calculate" onclick="loancalc()" />
				</p>

			</form>

			<p id="amount"></p>

			<p style="margin-top:40px;font-size:10px;border-top:1px solid #444;">
			<em>
			This loan calculator assumes compounding and payments occur monthly.  Your actual loan may vary but this estimate should still give you a good idea of about how much you can afford. The results of this loan payment calculator are for comparison purposes only.
			</em>
			</p>


        </div>
      </div>
    </section>

</div><!-- ./container -->
@stop
