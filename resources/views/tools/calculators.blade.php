@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Mortgage Calculator @parent
@stop

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/mortgageCalc/css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/sweetalert/sweetalert.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/alert.css') }}">
@stop

@section('scripts')
	<script type="text/javascript" src="{{ url('assets/vendors/sweetalert/sweetalert.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/vendors/mortgageCalc/js/pageLoad.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/vendors/mortgageCalc/js/modal.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/vendors/mortgageCalc/js/scrolltable.js') }}"></script>

	<script type="text/javascript" src="{{ url('assets/js/calculators/afford.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/js/calculators/rent.js') }}"></script>
@stop

@section('inline-scripts')
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('.resetPrice').tooltip();
});
var layout = '{{ $LAYOUT }}';
var siteUrl = '{{ $process_url }}';
@stop


{{-- Content --}}
@section('content')
<div class="container">


@include('tools._partials.mortgage')

<hr class="thick-3">

@include('tools._partials.affordability')

<hr class="thick-3">

@include('tools._partials.rent-vs-buy')

</div><!-- ./container -->
@stop
