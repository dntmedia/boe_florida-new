@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Mortgage Calculator @parent
@stop

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/sweetalert/sweetalert.css') }}">
@stop

@section('scripts')
	<script type="text/javascript" src="{{ url('assets/vendors/sweetalert/sweetalert.min.js') }}"></script>
@stop

@section('inline-scripts')
function loancalc() {
	var loanTerms=document.getElementById('loanterm').value;
	var interestRate=document.getElementById("loanrate").value;
	var loanpayments=document.getElementById("loanpayments").value;

	if (loanpayments === 'undefined') {loanpayments = 0.0;}  // DEFAULT VALUE IS $0
	if (loanTerms === 'undefined') {loanTerms = 30;} // DEFAULT VALUE IS 30 YEARS
	if (interestRate === 'undefined') {interestRate = 0.0025;} // default value = 3% yearly
	else {interestRate = (interestRate/100)/12;}

	loanTerms *= 12; // CONVERT TO MONTHS

	var ttlMoneyOut = loanpayments * loanTerms;
	var ttlInterest = (loanTerms/12) * interestRate;
	var moneyToInterest = ttlMoneyOut * ttlInterest
	ttlMoneyOut -= moneyToInterest;

	ttlMoneyOut.toFixed(2);

//	document.getElementById("amount").innerHTML = "Loan amount you should be able to afford $" + ttlMoneyOut + ".";
	sweetAlert('Loan amount you should be able to afford: $' + ttlMoneyOut + '.', '', '');
}
@stop


{{-- Content --}}
@section('content')
<div class="container">

    <section id="">
      <div class="container">
        <div class="row body-margin-top">
			<h1 class="text-center">Loan Affordability Calculator</h1>

<br>

<h4>
	How much house you can afford? Estimate the mortgage amount that best fits your budget with our new house calculator
</h4>

<br>

	<form>
	<p><strong>Desired Monthly Payment</strong>
	<br /><input type="text" id="loanpayments" value="1000" style="font-size:18px;padding:5px;" /></p>

	<p><strong>Applicable Interest Rate (% per Year)</strong>
	<br /><input type="number" id="loanrate" min='0' max='12' value="4.25" style="font-size:18px;width:90px;padding:5px 10px;" /> %</p>

	<p><strong>Desired Loan Term</strong>
	<br /><select id="loanterm" style="font-size:18px;padding:5px;">
	<option value="5">5 years</option>
	<option value="10">10 years</option>
	<option value="15">15 years</option>
	<option value="20">20 years</option>
	<option value="25">25 years</option>
	<option selected value="30">30 years</option>
	</select></p>

	<p><input type="button" value="Calculate" onclick="loancalc()" /></p>

	</form>
	<p id="amount"></p>

	<p style="margin-top:40px;font-size:10px;border-top:1px solid #444;">
	<em>
	This loan calculator assumes compounding and payments occur monthly.  Your actual loan may vary but this estimate should still give you a good idea of about how much you can afford. The results of this loan payment calculator are for comparison purposes only.
	</em>
	</p>


        </div>
      </div>
    </section>

</div><!-- ./container -->
@stop
