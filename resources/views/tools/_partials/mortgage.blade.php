<section id="">
<div class="container">
<div class="row body-margin-top">

<h1 class="text-center">
	Mortgage Calculator
</h1>

<br>

<h3>
	This calculator will show you the amortization schedule and breakdown of your payments made towards a home loan.
</h3>

<br>

<form class="inputF">

	<table id="selections" class="table">

		@if ( $ALLOWEMAIL == "yes" )
		<tr>

			<td>
				Send this as a PDF to your email?
			</td>

			<td>
				<input type="checkbox" class="sendEmail">
			</td>

			<td class="emailTD" colspan="2">
				<input type="text" class="email" placeholder="Your Email">
			</td>

		</tr>
		@endif

		<tr>

			<td>
				Purchase Price
			</td>

			<td>
				<span data-toggle="tooltip" data-placement="bottom"  title="The total purchase price of the home you wish to buy.">
					<i class="fa fa-question-circle-o fa-2x text-danger" aria-hidden="true"></i>
				</span>
			</td>

			<td class="entry">
				<input type="text" id="purchasePrice" class="priceHome inTxt" value="{{ number_format($DPRICE) }}">
			</td>

			<td>
				<label for="purchasePrice"><i class="fa fa-dollar fa-lg" style="font-size: 0">$</i></label>
			</td>

		</tr>

		<tr>

			<td>
				Interest Rate
			</td>

			<td>
				<span data-toggle="tooltip" data-placement="bottom"  title="The expected percent interest rate you will get on your mortgage.">
					<i class="fa fa-question-circle-o fa-2x text-danger" aria-hidden="true"></i>
				</span>
			</td>

			<td class="entry">
				<input type="text" id="interestRate" class="interest inTxt" value="{{ $DINT }}">
			</td>

			<td>
				<label for="interestRate"><i class="fa fa-percent fa-lg" style="font-size: 0">%</i></label>
			</td>

		</tr>

		<tr>

			<td>
				Down Payment
			</td>

			<td>
				<span data-toggle="tooltip" data-placement="bottom"  title="The percent down payment you wish to put towards the home.">
					<i class="fa fa-question-circle-o fa-2x text-danger" aria-hidden="true"></i>
				</span>
			</td>

			<td class="entry">
				<input type="text" id="downPayment" class="downPay inTxt" value="{{ $DDP }}">
			</td>

			<td>
				<label for="downPayment"><i class="fa fa-percent fa-lg" style="font-size: 0">%</i></label>
			</td>

		</tr>

		<tr>

			<td>
				Term
			</td>

			<td>
				<span data-toggle="tooltip" data-placement="bottom"  title="The number of years it will take to repay the loan amount (30 years is normal).">
					<i class="fa fa-question-circle-o fa-2x text-danger" aria-hidden="true"></i>
				</span>
			</td>

			<td class="entry">
				<select id="term" class="form-control term">
				<?php
for ($i = 5; $i <= 50; $i += 5) {
    echo '<option value="' . $i . '"';
    if ($i == $DTERM) {
        echo ' selected="selected" ';
    }
    echo '>' . $i . '</option>' . "\n";
}
?>
				</select>
			</td>

			<td>
				<label for="term"><i class="fa fa-calendar fa-lg" style="font-size: 0">Term</i></label>
				<i class="fa fa-calendar fa-lg"></i>
			</td>

		</tr>

		<tr>

			<td></td>

			<td class="buttonRow" colspan="3">
				<button class="submit btn btn-lg main-btn-form">Calculate</button>
				<button class="reset btn btn-lg main-btn-form-reset">Reset</button>
			</td>

		</tr>

	</table>

</form>


<div id="calcFooter">
	<h4>
		{{ $DISCLAIMER }}
	</h4>
</div>

<div id="schedule"></div>


<div class="waiting">
	<div class="center">
		<img src="{{ url('assets/vendors/mortgageCalc/css/images/ajax-loader.gif') }}" alt="loading">&nbsp;&nbsp;Please Wait...
	</div>
</div>


</div>
</div>
</section>
