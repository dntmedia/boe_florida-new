@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Reverse Mortgage @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<div class="container">

    <section id="why">
      <div class="#">

        <div class="container">
        <div class="row body-margin-top">
          <div class="col-md-12">
            <div class="product-content">
              <img src="{{ url('images/products/reverse_mortgage_icon_green.png') }}" alt="Bank of England Mortgage Reverse Mortgage Loans" class="fa img-responsive">
              <h1>Reverse Mortgage</h1>
            </div>

            <div class="who-we-are-body">
            <div class="product-content">
                <p class="lead">A reverse mortgage is a non-recourse loan, which means the borrower (or the borrower's estate) of a reverse mortgage will not owe more than the future loan balance or the value of the property, whichever is less. If the borrower or representatives of his or her estate choose to sell the property to pay off the reverse mortgage loan, no assets other than the home will be used to repay the debt. If the borrower or his or her estate wishes to retain the property, the balance of the loan must be paid in full.</p>
                <img src="{{ url('images/products/reverse_mortgage.jpg')}}" alt="Inquire about reverse mortgage loans today" class="img-responsive">
            </div>
            <h3>Let Your Home Take Care of You with a Reverse Mortgage/ Home Equity Conversion Mortgage </h3>
            <p>Reverse mortgages were created specifically for senior homeowners, allowing them to make the most of the equity they have acquired in their homes.</p>
            <p>With a reverse mortgage, you borrow against the equity you have established in your home and do not need to repay the loan for as long as you live in the home as your primary residence, maintain your home in good condition, and pay property taxes and insurance. You can live in your home and enjoy making no monthly principal and interest mortgage payments.</p>
            <p>Depending on your financial situation, a reverse mortgage has the potential to help you stay in your home and still meet your financial obligations.</p>
            <p>We realize that reverse mortgages may not be right for everyone, give us a call so we can help walk you through the process and answer any questions you may have.</p>

            <h3>Reverse Mortgages vs. Traditional Mortgage or Home Equity Loans</h3>
            <p>A reverse mortgage is the opposite of a traditional mortgage. With a traditional mortgage, you borrow money and make monthly principal and interest mortgage payments. With a reverse mortgage, however, you receive loan proceeds based on the value of your home, the age of the youngest borrower, and the interest rate of your loan. You do not make monthly principal and interest mortgage payments for as long as you live in, maintain your home in good condition, and pay property taxes and insurance. The loan must be repaid when you pass away, sell your home, or no longer live in the home as your primary residence.</p>
            <p>Home Equity Conversion Mortgage (HECM)</p>
            <ul>
              <li>A Home Equity Conversion Mortgage, or HECM, is the only reverse mortgage insured by the U.S. Federal Government, and is only available through an FHA-approved lender.</li>
            </ul>

            <h3>How Much Can Be Borrowed?</h3>
            <p>In general, the more your home is worth, the older you are, and the lower the interest rate, the more you will be able to borrow.</p>
            <p>The maximum amount that can be borrowed on a particular loan program is based on these factors:</p>
            <ul>
              <li>The age of the youngest borrower at the time of the loan</li>
              <li>The appraised value of the home</li>
              <li>Current interest rates</li>
            </ul>

            <h3>Initial Eligibility Requirements for Reverse Mortgages</h3>
            <p>The initial eligibility requirements are quite simple.</p>
            <ul>
              <li>Homeowners must be 62 years of age or older and occupy the property as their primary residence</li>
              <li>The property may be a Single family or a 2-4 Unit property, Townhome, or FHA-approved Condominium</li>
              <li>The home must meet minimum FHA property standards</li>
              <li>Borrower cannot be delinquent on any federal debt</li>
              <li>Completion of HECM counseling</li>
            </ul>
            <hr>
            <p class="text-center">Contact one of our Reverse Mortgage Specialists today to get started!</p>
            <div class="specialists">
              <div class="col-md-6">
                <div class="thumbnail">
                    <img src="{{ url('images/employee/alt/bmarquette-sm.jpg') }}" style="border: 1px solid black" alt="Brian Marquette Reverse Mortgage Specialist">
                  <div class="caption">
                    <h3>Brian Marquette</h3>
                    <h4>Reverse Mortgage Specialist</h4>
                    <p>NMLS # 327095</p>
                    <p>Office: (941) 361-1280 ext 127</p>
                    <p>Mobile: (941) 232-4359</p>
                    <p><a href="bmarquette@boemortgage.com">bmarquette@boemortgage.com</a></p>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="thumbnail">
                    <img src="{{ url('images/employee/alt/gbarich-sm.jpg') }}" style="border: 1px solid black" alt="Glenn Barich Reverse Mortgage Specialist">
                  <div class="caption">
                    <h3>Glenn Barich</h3>
                    <h4>Reverse Mortgage Specialist</h4>
                    <p>NMLS # 1064156</p>
                    <p>Office: (941) 361-1280 ext 117</p>
                    <p>Mobile: (941) 724-4020</p>
                    <p><a href="gbarich@boemortgage.com">gbarich@boemortgage.com</a></p>
                  </div>
                </div>
              </div>
            </div>
            <ul class="pager">
                <li class="previous"><a href="/products/bridge-loans">Previous</a></li>
                <li class="next"><a href="/products/fha-loans">Next</a></li>
              </ul>
            </div>
        </div>
      </div>
    </section>

</div><!-- ./container -->
@stop
