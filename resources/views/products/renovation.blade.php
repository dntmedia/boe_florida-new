@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Renovation @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<div class="container">

   <section id="why">
      <div class="#">
      
        <div class="container">
        <div class="row body-margin-top">
          <div class="col-md-12">
            <div class="product-content">
              <img src="{{ url('images/products/renovation_loan_icon_green.png') }}" alt="Bank of England Mortgage Renovation Loans" class="fa img-responsive">
              <h1>Renovation Loans</h1>
            </div>
            <div class="who-we-are-body">
              <div class="product-content">
                <img src="{{ url('images/products/renovation.jpg')}}" class="img-responsive" alt="Renovation loans give you <i>flexibility</i>. Contact your local BOE Branch to get started.">
                <p>Renovation loans give you <i>flexibility</i>. Whether you are building, buying, or refinancing your home, a renovation loan allows you to add a room, remodel, and upgrade.  Save by financing renovation costs into your mortgage rather than racking up credit card bills or dipping into your savings. With one loan, there's only one application, one set of fees, one closing and one monthly payment.  Improvements may include repairs and rennovations that adds value to your home, including a garage, swimming pool and energy-efficiency upgrades.</p>
              </div>

              <p>Contact us today at <a href="telto:9413611280">(941) 361-1280</a> to get started or <a href="https://3146532938.mortgage-application.net/WebApp/Start.aspx" target="_blank">start an application today</a>.</p>

              <ul class="pager">
                <li class="previous"><a href="/products/construction">Previous</a></li>
                <li class="next"><a href="/products/bridge-loans">Next</a></li>
              </ul>
            </div>
        </div>
      </div>
    </section>

</div><!-- ./container -->
@stop
