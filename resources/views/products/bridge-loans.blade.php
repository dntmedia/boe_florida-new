@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Bridge Loans @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<div class="container">

    <section id="why">
      <div class="#">

        <div class="container">
        <div class="row body-margin-top">
          <div class="col-md-12">
            <div class="product-content">
              <img src="{{ url('images/products/bridge_loan_icon_green.png') }}" alt="Bank of England Mortgage Bridge Loans" class="fa img-responsive">
              <h1>Bridge Loans</h1>
            </div>
            <div class="who-we-are-body">
              <div class="product-content">
                <p>Bank of England Mortgage provides bridge loans which are temporary mortgages that provide a downpayment for a new home before completing the sale of your current residence. Many buyers today would like to sell their current home to provide a downpayment on the next one. But timing can be a problem. You can’t always make that happen. For instance, sales fall through, or the perfect home for you has multiple offers and a seller who wants to close fast. This is where bridge loans come in handy.  They can be a great tool when purchasing your new dream home, before you sell your current one.</p>
                <img src="{{ url('images/products/bridge_loan.jpg')}}" class="img-responsive" alt="Bridge loans are temporary mortgages that provide a downpayment for a new home before completing the sale of your current residence. Contact your local BOE Mortgage branch for more information.">
              </div>

              <p>There are two ways a bridge loan can be structured. The first method is to pay off your old mortgage, and provide additional cash for your new home downpayment. The second scenario is more like a home equity loan. Instead of replacing the existing mortgage on your old home, you take a smaller bridge loan that just covers the downpayment on the new property.</p>

              <p>Contact us today at <a href="telto:9413611280">(941) 361-1280</a> to get started or <a href="https://3146532938.mortgage-application.net/WebApp/Start.aspx" target="_blank">start an application today</a>.</p>

              <ul class="pager">
                <li class="previous"><a href="/products/renovation">Previous</a></li>
                <li class="next"><a href="/products/reverse-mortgage">Next</a></li>
              </ul>
            </div>
        </div>
      </div>
    </section>

</div><!-- ./container -->
@stop
