@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
FHA Loans @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<div class="container">

    <section id="why">
      <div class="#">

        <div class="container">
        <div class="row body-margin-top">
          <div class="col-md-12">
            <div class="product-content">
              <img src="{{ url('images/products/fha_loan_icon_green.png') }}" alt="Bank of England Mortgage FHA Loans" class="fa img-responsive">
              <h1> FHA Loan</h1>
            </div>
            <div class="who-we-are-body">
              <div class="product-content">
                <p>We are pleased to announce that we have lowered our minimum credit score standard on all FHA loan products. There are countless reasons why an FHA loan might be right for you, but here at Bank of England Mortgage, we believe that these four features are what really sets FHA loans apart.</p>
                <img src="{{ url('images/products/house_in_nest.jpg')}}" class="img-responsive" alt="There are countless reasons why an FHA loan might be right for you, but here at Bank of England Mortgage, we believe that these four features are what really sets FHA loans apart.">
              </div>

          <h3>Credit Flexible</h3>
          <p>FHA loans are not score driven. Instead, they are written in a way that provides the borrower the benefit of the doubt that there had been, at some point in their past, circumstances beyond their control, and as long as the borrower has recovered from those circumstances in a reasonable manner, they're generally going to be credit-eligible for an FHA loan.</p>

          <h3>Great Rates and Low Monthly Mortgage Insurance</h3>
          <p>A distinct advantage of an FHA insured loan, as compared to a conforming loan, is great interest rates and lower monthly mortgage insurance (MI). Depending on the program, standard FHA loan interest rates are usually better than a conforming 30-Year Fixed loan.</p>

          <h3>Safest ARM Currently Available on the Market</h3>
          <p>FHA guidelines give you the option of doing hybrid Adjustable Rate Mortgages (ARM), including a 3/1 ARM and a one year ARM that has the lowest adjustment caps of any ARM in the industry.</p>

          <h3>Variety of Property Types Allowed</h3>
          <p>While FHA Guidelines do require that the property be Owner Occupied (OO), they do allow you to purchase condos, planned unit developments, manufactured homes, and 1-4 family residences, in which the borrower intends to occupy one part of the multi-unit residence.</p>

          <h3>Streamlined Refinance and Assumable Loans</h3>
          <p>One of the most important advantages of an FHA loan is the ability for the loan to be assumed. This gives the buyer a significant advantage in a high interest rate market. FHA loans are eligible for streamlined refinance, a program HUD offers that allows the borrower to easily refinance the loan to reduce their interest rate and lower their monthly payment.</p>

          <hr>

          <h3 class="accord-heading">FHA Loan FAQs</h3>
          <p>If you're in search of information on FHA loans, we can help! Here are our most frequently asked questions about FHA home loans.</p>
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                  <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    What are FHA loans?
                  </a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  FHA stands for Federal Housing Administration. A Federal Housing Administration loan provides low-cost insured home mortgage loans that suit a variety of purchasing options. Whether you're buying a home or want or refinance your mortgage, FHA loans might be right for you. If you're unsure about your credit rating, or have concerns about a down payment, our FHA loans can give you piece of mind with super low closing costs and flexible payment options.
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    How do I qualify for FHA loans?
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                  To be eligible for Federal Housing Administration loans, your monthly housing costs (mortgage principal and interest, property taxes, and insurance) must meet a specified percentage of your gross monthly income. Your credit background will be fairly considered. You must be able to make a down payment, cover closing costs and have enough income to pay your monthly debt. Call Bank of England Mortgage to discuss your FHA loan eligibility.
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    What is the cost of FHA home loans?
                  </a>
                </h4>
              </div>
              <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                  Interest rates on all home mortgage loans vary according to the economic climate of the country. FHA home loans offer better than standard rates and lower monthly mortgage insurance premiums. Depending on the program, standard FHA home loan interest rates are usually better than a conforming 30-year fixed loan. Call us today for more information on FHA loans.
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingFour">
                <h4 class="panel-title">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                    Can I use FHA loans for home repairs?
                  </a>
                </h4>
              </div>
              <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                <div class="panel-body">
                  Yes! Simple home improvements can be financed with a Federal Housing Administration loan from Bank of England Mortgage. Roof repair, disability accessibility improvements, flooring refurbishment, or the purchase of new appliances are just a few eligible improvements you could make with FHA loans. Ask our experts if your project qualifies for an FHA loan.
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingFive">
                <h4 class="panel-title">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                    Are FHA mortgage loans assumable and can they be refinanced?
                  </a>
                </h4>
              </div>
              <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                <div class="panel-body">
                  Yes & yes! One of the greatest advantages of our FHA mortgage loans is the ability for the loan to be assumed. This allows the buyer a significant advantage in a high-interest rate market, and affords you an excellent sales advantage over your competition. FHA mortgage loans are also eligible for streamlined refinancing, offering you a lower than standard interest rate, which can lower your monthly payment. Streamlined FHA mortgage loans are an excellent selling feature when you're ready to move up or move on.
                </div>
              </div>
            </div>
          </div>

          <p>Contact us today at <a href="telto:9413611280">(941) 361-1280</a> to get started or <a href="https://3146532938.mortgage-application.net/WebApp/Start.aspx" target="_blank">start an application today</a>.</p>

          <ul class="pager">
                <li class="previous"><a href="/products/reverse-mortgage">Previous</a></li>
                <li class="next"><a href="/products/va-loans">Next</a></li>
              </ul>
        </div>
      </div>
    </section>

</div><!-- ./container -->
@stop
