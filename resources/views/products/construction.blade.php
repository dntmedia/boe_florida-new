@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Construction @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<div class="container">

    <section id="why">
      <div class="#">

        <div class="container">
        <div class="row body-margin-top">
          <div class="col-md-12">
            <div class="product-content">
              <img src="{{ url('images/products/construction_loan_icon_green.png') }}" alt="Bank of England Mortgage Construction Loans" class="fa img-responsive">
              <h1>Construction</h1>
            </div>
            <div class="who-we-are-body">
              <div class="product-content">
                <div>
                  <h2>FHA and VA One-Time-Close Construction Loan</h2>
                  <p class="lead">Why worry about re-qualifying or incurring additional costs?  Designed for manufactured, modular, and stick built housing, this program offers an all-in-one financing option for construction, lot purchase, and permanent mortgage funding with one closing.  The permanent loan is closed before construction begins</p>
                  <p><strong>No re-qualification.  No second appraisal.</strong></p>
                </div>
                <img src="{{ url('images/products/construction.jpg')}}" class="img-responsive" alt="Contact your local BOE Mortgage branch to learn more about FHA and VA One-Time-Close Construction Loan">
              </div>
            </div>

              <div class="row">
                <div class="col-md-6">
                  <p><u>Program Benefits</u></p>
                  <ul>
                    <li>Up to 96.5% LTV through FHA or 100% through VA (not including funding fee)</li>
                    <li>No payments due from borrower during construction</li>
                    <li>No re-qualification once construction is complete</li>
                    <li>Single closing reduces total costs</li>
                  </ul>
                </div>
                <div class="col-md-6">
                  <p><u>Financing Eligibility</u></p>
                  <ul>
                    <li>Available through FHA & VA</li>
                    <li>A minimum qualifying credit score</li>
                    <li>15 and 30 Year Fixed Rates available</li>
                    <li>1 Unit Single Family Residences, Doublewide and Triplewide Manufactured Homes and Modular Homes permitted</li>
                  </ul>
                </div>
              </div>

              <p>Contact us today at <a href="telto:9413611280">(941) 361-1280</a> to get started or <a href="https://3146532938.mortgage-application.net/WebApp/Start.aspx" target="_blank">start an application today</a>.</p>

              <ul class="pager">
                <li class="previous"><a href="/products/jumbo-loans">Previous</a></li>
                <li class="next"><a href="/products/renovation">Next</a></li>
              </ul>
            </div>
        </div>
      </div>
    </section>

</div><!-- ./container -->
@stop
