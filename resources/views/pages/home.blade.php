@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Home @parent
@stop

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/lightslider.css') }}">
@stop

@section('scripts')
	<script type="text/javascript" src="{{ url('assets/vendors/lightslider/js/lightslider.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/js/light_slider.js') }}"></script>
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
    <section id="lead">
      <div class="row">
        <div class="col-lg-12">
          <div id="lead-image">
            <img src="{{ url('images/home/slider_1.jpg') }}" alt="Bank of England Mortgage Florida" class="img-responsive">
          </div>
          <div id="lead-content" class="animated slideInRight">
            <h1>Welcome to Bank of England Mortgage Florida</h1>
            <p>Helping reach your dreams since 1898</p>
          </div>
        </div>
      </div>
    </section>


    <section id="products">
      <div class="container">
      <h2 class="text-uppercase">Loan Programs</h2>


        <ul id="lightSlider">
          <li>
            <div class="thumbnail product-thumb">
                <a href="/products/reverse-mortgage"><img src="{{ url('images/home/reverse_mortgage_icon.png') }}" class="fa" alt="Bank of England Mortgage Reverse Mortgage"></a>
                <div class="caption">
                  <h3 class="text-center">Reverse Mortgage</h3>
                  <p class="text-center"><a class="btn main-btn product-btn" href="/products/reverse-mortgage">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
                </div><!-- /.caption -->
              </div><!-- /.thumbnail -->
          </li>
          <li>
            <div class="thumbnail product-thumb">
                <a href="/products/fha-loans"><img src="{{ url('images/home/fha_loan_icon.png') }}" class="fa" alt="Bank of England Mortgage FHA Loans"></a>
                <div class="caption">
                  <h3 class="text-center">FHA Loans</h3>
                  <p class="text-center"><a class="btn main-btn product-btn" href="/products/fha-loans">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
                </div><!-- /.caption -->
              </div><!-- /.thumbnail -->
          </li>
          <li>
            <div class="thumbnail product-thumb">
                <a href="/products/va-loans"><img src="{{ url('images/home/va_loan_icon.png') }}" class="fa" alt="Bank of England Mortgage VA Loans"></a>
                <div class="caption">
                  <h3 class="text-center">VA Loans</h3>
                  <p class="text-center"><a class="btn main-btn product-btn" href="/products/va-loans">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
                </div><!-- /.caption -->
              </div><!-- /.thumbnail -->
          </li>
          <li>
            <div class="thumbnail product-thumb">
                <a href="/products/vacation"><img src="{{ url('images/home/vacation_loan_icon.png') }}" class="fa" alt="Bank of England Mortgage Vacation/Second Homes"></a>
                <div class="caption">
                  <h3 class="text-center">Vacation/Second Home</h3>
                  <p class="text-center"><a class="btn main-btn product-btn" href="/products/vacation">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
                </div><!-- /.caption -->
              </div><!-- /.thumbnail -->
          </li>
          <li>
            <div class="thumbnail product-thumb">
                <a href="/products/usda-loans"><img src="{{ url('images/home/usda_loan_icon.png') }}" class="fa" alt="Bank of England Mortgage USDA Loans"></a>
                <div class="caption">
                  <h3 class="text-center">USDA Loans</h3>
                  <p class="text-center"><a class="btn main-btn product-btn" href="/products/usda-loans">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
                </div><!-- /.caption -->
              </div><!-- /.thumbnail -->
          </li>
          <li>
            <div class="thumbnail product-thumb">
                <a href="/products/jumbo-loans"><img src="{{ url('images/home/jumbo_loan_icon.png') }}" class="fa" alt="Bank of England Mortgage Jumbo Loans"></a>
                <div class="caption">
                  <h3 class="text-center">Jumbo Loans</h3>
                  <p class="text-center"><a class="btn main-btn product-btn" href="/products/jumbo-loans">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
                </div><!-- /.caption -->
              </div><!-- /.thumbnail -->
          </li>
          <li>
            <div class="thumbnail product-thumb">
                <a href="/products/construction"><img src="{{ url('images/home/construction_loan_icon.png') }}" class="fa" alt="Bank of England Mortgage Construction (one-time close)"></a>
                <div class="caption">
                  <h3 class="text-center">Construction <small>(one-time close)</small></h3>
                  <p class="text-center"><a class="btn main-btn product-btn" href="/products/construction">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
                </div><!-- /.caption -->
              </div><!-- /.thumbnail -->
          </li>
          <li>
            <div class="thumbnail product-thumb">
                <a href="/products/renovation"><img src="{{ url('images/home/renovation_loan_icon.png') }}" class="fa" alt="Bank of England Mortgage Renovation"></a>
                <div class="caption">
                  <h3 class="text-center">Renovation</h3>
                  <p class="text-center"><a class="btn main-btn product-btn" href="/products/renovation">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
                </div><!-- /.caption -->
              </div><!-- /.thumbnail -->
          </li>
          <li>
            <div class="thumbnail product-thumb">
                <a href="/products/bridge-loans"><img src="{{ url('images/home/bridge_loan_icon.png') }}" class="fa" alt="Bank of England Mortgage Bridge Loans"></a>
                <div class="caption">
                  <h3 class="text-center">Bridge Loans</h3>
                  <p class="text-center"><a class="btn main-btn product-btn" href="/products/bridge-loans">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
                </div><!-- /.caption -->
              </div><!-- /.thumbnail -->
          </li>
        </ul>
      </div>
    </section>
    <section id="team">
      <div class="row">
        <div class="col-lg-12">
          <div id="team-photo">
            <img src="{{ url('images/home/florida_team.jpg') }}" alt="Bank of England Mortgage Team" class="img-responsive">
          </div>
          <div id="team-content" class="animated fadeInRightBig">
            <h3><a href="/about/our-team">Meet the team <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a></h3>
          </div>
          
        </div>
      </div>
    </section>

    <section id="tools">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="thumbnail">
                <a href="/about/who-we-are" target="_blank"><img src="{{ url('images/home/sarasota_thumbnail.jpg') }}" alt="About Sarasota, Florida" class="img-responsive"></a>
                <div class="caption">
                  <h3 class="text-center">About Sarasota</h3>
                  <p></p>
                  <p class="text-center"><a class="btn main-btn" href="/about/who-we-are#about_sarasota">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
                </div><!-- /.caption -->
              </div><!-- /.thumbnail -->
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="thumbnail">
                <a href="/about/partners"><img src="{{ url('images/home/partners_thumbnail.jpg') }}" alt="BOE Mortgage Florida Partners"></a>
                <div class="caption">
                  <h3 class="text-center">Our Partners</h3>
                  <p></p>
                  <p class="text-center"><a class="btn main-btn" href="/about/partners">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
                </div><!-- /.caption -->
              </div><!-- /.thumbnail -->
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="thumbnail">
                <a href="/tools/fastapp"><img src="{{ url('images/home/fastapp_thumbnail.jpg') }}" alt="BOE FastApp available for download"></a>
                <div class="caption">
                  <h3 class="text-center">BOE FastApp</h3>
                  <p></p>
                  <p class="text-center"><a class="btn main-btn" href="/tools/fastapp">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
                </div><!-- /.caption -->
              </div><!-- /.thumbnail -->
          </div>
        </div>
      </div>
    </section>

    <section style="padding: 3rem 0">
      <div class="row">
        <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-lg-push-8 col-md-push-8 col-sm-push-8 text-center">
            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FBOEFlorida%2F&tabs=timeline&width=340&height=575&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=false&appId" width="340" height="575" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-lg-pull-4 col-md-pull-4 col-sm-pull-4">   
        <p class="social-title">Like, Share, and Explore with Us. We're passionate about our community.</p>
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" width="100%" height="575" src="https://www.youtube.com/embed/OX05oIUqNq0" frameborder="0" allowfullscreen></iframe>
              </div>
            </div>
          </div>  
        </div>
        
      </div>
      
    </section>
    <!-- <section style="padding:4rem 0;">
      <div class="row">
        <div class="container">
          <div class="feature-content">
            <div class="col-lg-7 col-md-7">
            
            </div>
            <div class="col-lg-5 col-md-5 vid-section">
              <div class="vid-container">
                <iframe id="vid_frame" src="https://www.youtube.com/embed/U881aEwMABM?&rel=0&showinfo=0&autohide=1" width="560" height="315" frameborder="0"></iframe>
              </div>
              <div class="vid-item" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/U881aEwMABM?autoplay=1&rel=0&showinfo=0&autohide=1'">
                <div class="thumb">
                  <img src="http://img.youtube.com/vi/U881aEwMABM/0.jpg" class="img-responsive">
                </div>
                <div class="desc">
                  Sarasota is Our Home
                </div>
              </div>
              <div class="vid-item" onClick="document.getElementById('vid_frame').src='https://www.youtube.com/embed/OX05oIUqNq0?autoplay=1&rel=0&showinfo=0&autohide=1'">
                <div class="thumb">
                  <img src="http://img.youtube.com/vi/OX05oIUqNq0/0.jpg" class="img-responsive">
                </div>
                <div class="desc">
                  We Are Bank of England Mortgage
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> -->

</div><!-- ./container -->
@stop
