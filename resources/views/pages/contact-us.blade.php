@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Contact Us Today @parent 
@stop

@section('styles')
@stop

@section('inline-styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<section>
  

<div style="position: relative;">
	<img src="{{ url('images/home/slider_3.jpg') }}" alt="Bank of England Mortgage Florida" class="">
  <div class="contactUsBox">
    <div class="contactUsBox-text">
      <h1 class="text-center">Contact Us Today</h1>
      <h2 class="text-center">By Phone: 941-361-1280 <span><br> OR <br></span> Fill out form below and we will contact you to get started</h2>

      @include('_partials.contact_only2')
    </div>
  </div>
</div>

</section>

@stop
