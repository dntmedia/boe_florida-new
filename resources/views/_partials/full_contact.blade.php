<section id="contact" class="contact-us">

<div class="container">
<div class="row">

      <div class="col-md-4">
        <img src="{{ url('images/site/_footer/boe_logo_green.png') }}" alt="Bank of England Mortgage Florida">
        <p style="font-size: 12px">Bank of England Mortgage is a division of the Bank of England, a locally owned community bank located in England, Arkansas. We provide our clients with the expertise and services that are traditionally offered by the largest financial services institutions in the country - with the integrity of a local community bank.</p>
        <address style="font-size: 12px">
          <a href="https://goo.gl/maps/RnPoTtkTFB12"><i class="fa fa-map-marker" aria-hidden="true"></i> 235 N Orange Avenue Suite 200 |
          Sarasota, FL 34236 </a><br />
          <i class="fa fa-phone-square" aria-hidden="true"></i> Tel: (941) 361-1280 | 
          <i class="fa fa-fax" aria-hidden="true"></i>Fax: (866) 569-1699
        </address>
<!--         <div class="social-links">
          <ul class="">
            <li><a href="telto:9413611280" target="_blank"> <i class="fa fa-phone" aria-hidden="true"></i>Call Us</a></li>
            <li><a href="http://sarasota.boemortgage.com/" target="_blank"> <i class="fa fa-file-text" aria-hidden="true"> </i>Apply Now</a></li>
            <li><a href="mailto:omarogi@boemortgage.com" target="_blank"> <i class="fa fa-envelope" aria-hidden="true"> </i>Email Us</a></li>
            <li><a href="https://www.facebook.com/BOEFlorida/" target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"> </i>Like Us</a></li>
          </ul>
        </div> -->

      </div>

      <div class="col-md-8">
		@include('_partials.contact_only')
      </div>

</div><!-- ./row -->
</div><!-- ./container -->

</section>
