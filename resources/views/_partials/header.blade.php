<div class="container">

  <!-- Static navbar -->
    <nav class="navbar navbar-custom navbar-default navbar-fixed-top">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">
            <img src="{{ url('images/site/boe_logo1.png') }}" alt="Bank of England Mortgage Florida"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="/"><i class="fa fa-home fa-fw" aria-hidden="true"></i> Home <span class="sr-only">(current)</span></a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-users fa-fw" aria-hidden="true"></i> About Us <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="/about/who-we-are">Who We Are</a></li>
                  <li><a href="/about/our-team">Our Team</a></li>
                  <li><a href="/about/testimonials">Testimonials</a></li>
                  <li><a href="/about/partners">Our Partners</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-th-large fa-fw" aria-hidden="true"></i> Loan Programs <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="/products/reverse-mortgage">Reverse Mortgage</a></li>
                  <li><a href="/products/fha-loans">FHA Loans</a></li>
                  <li><a href="/products/va-loans">VA Loans</a></li>
                  <li><a href="/products/vacation">Vacation/Second Home Mortgage</a></li>
                  <li><a href="/products/usda-loans">USDA Loans</a></li>
                  <li><a href="/products/jumbo-loans">Jumbo Loans</a></li>
                  <li><a href="/products/construction">Construction (one-time close)</a></li>
                  <li><a href="/products/renovation">Renovation</a></li>
                  <li><a href="/products/bridge-loans">Bridge Loans</a></li>
                </ul>
              </li>
              <li><a href="http://sarasota.boemortgage.com/" class="orange-highlight" target="_blank"><i class="fa fa-file-text fa-fw" aria-hidden="true"></i> Apply Now</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cogs fa-fw" aria-hidden="true"></i> Tools <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="/tools/fastapp">BOE FastApp</a></li>
                  <li class="divider"></li>
                  <li><a href="/tools/mortgage-calculator">Mortgage Calculator</a></li>
                  <li><a href="/tools/mortgage-calculator#affordability">Affordability Calculator</a></li>
                  <li><a href="/tools/mortgage-calculator#rent-vs-buy">Rent vs Buy Calculator</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle btn green-highlight" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-phone fa-fw" aria-hidden="true"></i> CALL NOW 941-361-1280</a>
                <ul class="dropdown-menu">
                  <li><a href="/contact-us" class="btn green-highlight">Contact Us Today</a></li>
                </ul>
              </li>
              
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

</div> <!-- /container -->
