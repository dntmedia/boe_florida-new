
<!-- <p class="contactus-head">Contact Us Today</p> -->

<form id="contact-form" method="post" action="/send-contact" role="form">

    <div class="messages"></div>

    <div class="controls">

        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    <label for="form_name">First Name *</label>
                    <input id="form_name" type="text" name="name" class="form-control" placeholder="Please enter your first name *" required="required" data-error="Firstname is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="form_lastname">Last Name *</label>
                    <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Please enter your last name *" required="required" data-error="Lastname is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="form_email">Email *</label>
                    <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="form_phone">Phone</label>
                    <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Please enter your phone">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>

        <div class="row">



            <div class="col-md-12 contact_hidden">
                <div class="form-group">
                    <label for="form_address">Address</label>
                    <input id="form_address" type="tel" name="address" class="form-control" placeholder="Please enter your address">
                    <div class="help-block with-errors"></div>
                </div>
            </div>


            <div class="col-md-12  col-sm-12 contact_move_up">
                <input type="submit" class="btn main-btn-form fa fa-arrow-circle-right fa-2x" value="Send Message &raquo;">
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 col-sm-12">
                <p class="text-muted" style="color: #fff"><strong>*</strong> These fields are required.</p>
            </div>
        </div>
    </div>

</form>
