@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Need Credit Counseling Help? @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<div class="container">

    <section style="margin-top: 95px">
     <div class="container">
        <h2>
        	Need Credit Counseling Help?
        </h2>
         <p class="text-uppercase">IMPORTANT INFORMATION ABOUT PROCEDURES FOR OPENING OR CHANGING AN ACCOUNT WITH BANK OF ENGLAND MORTGAGE</p>

        <p>If you wish to consult a third party for credit counseling services, please check the website of the <a href="http://portal.hud.gov/hudportal/HUD?src=/program_offices/housing/sfh/hcc/hcc_home" target="_blank">U.S. Department of Housing and Urban Development</a> to locate a HUD approved counselor who will not charge you for their services.</p>

        <p>If you are a military service member or a dependent of a military service member, and require counseling assistance, please call the Military One-Source Toll-Free number at 1(800) 342-9647.</p>
      </div>
    </section>

</div><!-- ./container -->
@stop
