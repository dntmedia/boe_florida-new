@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Security @parent 
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<div class="container">

    <section style="margin-top: 95px">
     <div class="container">
      <h2>
      	Security
      </h2>
       <p class="lead">Bank of England Mortgage is dedicated to protecting your privacy. All information you submit is treated with the highest level of confidence. We do not share your confidential information with any other party. The information submitted will be used solely to evaluate your credit application.</p>

       <h3>Secure Online Application</h3>

       <p>Bank of England Mortgage has now made it even easier for you to apply for your loan online. Our new state-of-the-art, secure online loan application allows our loan specialists to securely interact with our borrowers online saving you time and money.</p>

       <p>Our new online application also makes it possible for us to close your loan in record time through its advanced electronic submission features.</p>

       <ul>
         <li>True 128-bit strong SSL encryption</li>
         <li>AICPA WebTrust compliant</li>
       </ul>
       <p>Your information is secure when you apply with Bank of England Mortgage. The entire application is housed on our secure server and will not be shared with anyone not directly involved with your loan process.</p>

     </div>
    </section>
</div><!-- ./container -->
@stop
