@extends('_layouts.app')

{{-- Web site Title --}}
@section('title')
Contact Us @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')


    <section id="contact-banner">
      <div class="container">
        <div class="row">
          <div class="contact-content">
            <div class="col-lg-8 col-lg-offset-2">
              <div id="success-msg">
                <h1>  {{  $message }} </h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

@stop
