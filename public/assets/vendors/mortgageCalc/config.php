<?php
/**
 * Config file for Mortgage Calculator
 * This file holds settings that affect the mortgage calculator tool.
 *
 **/

// Defaults for initial calculator
define('DPRICE', '224000.00');
define('DINT', '5');
define('DDP', '20');
define('DTERM', '30');

// Other factors (taxes, insurance, pmi)
define('HIR', '56'); // Homeowners Insurance Rate
define('PMI', '56'); // Private Mortgage Insurance

// Settings
define('LAYOUT', 'modal'); // amortization schedule modal, div or popup
define('WEBSITE', 'www.yourwebsite.com'); // Used in pdf emails to let your customers know where they came from
define('URL', 'http://www.yourwebsite.com/mortgageCalc'); // Used for scripts and file reference locations (include http://), no trailing slashes
define('EMAIL', ''); // Used for bcc to you so you can contact the customer
define('FROMEMAIL', 'info@BOEMortgage.com'); // The email address that shows in the from field
define('ALLOWEMAIL', 'no'); // Allow email to send report to customer
define('SHOWBLURB', 'yes'); // Option to give information about other costs before schedule

// Disclaimer can say what you want
define('DISCLAIMER', 'These calculations are estimates only and do not constitute an offer to finance your loan. There is no warranty for the accuracy of the results or the relationship to your personal financial situation.');
