function affordCalc() {
	var loanTerms=document.getElementById('loanterm').value;
	var interestRate=document.getElementById("loanrate").value;
	var loanpayments=document.getElementById("loanpayments").value;

	if (loanpayments === 'undefined') {loanpayments = 0.0;}  // DEFAULT VALUE IS $0
	if (loanTerms === 'undefined') {loanTerms = 30;} // DEFAULT VALUE IS 30 YEARS
	if (interestRate === 'undefined') {interestRate = 0.0025;} // default value = 3% yearly
	else {interestRate = (interestRate/100)/12;}

	loanTerms *= 12; // CONVERT TO MONTHS

	var ttlMoneyOut = loanpayments * loanTerms;
	var ttlInterest = (loanTerms/12) * interestRate;
	var moneyToInterest = ttlMoneyOut * ttlInterest
	ttlMoneyOut -= moneyToInterest;

	ttlMoneyOut.toFixed(2);

//	document.getElementById("amount").innerHTML = "Loan amount you should be able to afford $" + ttlMoneyOut + ".";
	sweetAlert('Loan amount you should be able to afford: $' + ttlMoneyOut + '.', '', '');
}