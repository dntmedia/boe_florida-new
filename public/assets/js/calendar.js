(function($) {

var date = moment().format('YYYY-MM-DD');
var month = moment().format('MM');

    $('#calendar').fullCalendar({

        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,listYear'
        },
        navLinks: true, // can click day/week names to navigate views
        eventLimit: true, // allow "more" link when too many events
        displayEventTime: true, // don't show the time column in list view

        defaultView: 'listYear',
        defaultDate: date,
        gotoDate: date,

        googleCalendarApiKey: 'AIzaSyAhMpdYETAU0CsuT4fl0G75rKYdgmkYsJc',

        events: 'en.usa#holiday@group.v.calendar.google.com',

        eventSources: [
            {
                googleCalendarId: 'k5bpecqitm7e2qc9n437ch6bp4@group.calendar.google.com'
            }
        ],

      eventClick: function(event) {
        // opens events in a popup window
        window.open(event.url, 'gcalevent', 'width=700,height=600');
        return false;
      },

      loading: function(bool) {
        $('#loading').toggle(bool);
      }

    });

}(jQuery));
