$(document).ready(function() {

  jQuery('#vmap').vectorMap({
  map: 'usa_en',
    backgroundColor: null,
    borderColor: '#215732',
    borderOpacity: 0.9,
    borderWidth: 1,
    color: '#aaa',
    enableZoom: false,
    hoverColor: '#215732',
    hoverOpacity: null,
    scaleColors: ['#C8EEFF', '#006491'],    
    normalizeFunction: 'linear',
    // colors: {
    //   mo: '#666666',
    //   fl: '#666666',
    //   or: '#666666'
    // },
  onResize: function (element, width, height) {
    console.log('Map Size: ' +  width + 'x' +  height);
  },
  onRegionClick: function(element, code, region)   {
    window.location='/find-branch/'+code.toUpperCase();
    }});

});
