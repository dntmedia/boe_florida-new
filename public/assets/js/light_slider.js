$(document).ready(function() {

	$("#lightSlider").lightSlider({
        item:4,
        // loop:false,
        slideMove:2,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 400, //ms'
        auto: true,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        pauseOnHover: true,
        controls: true,
        adaptiveHeight: false,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:6,
                  }
            },
            {
                breakpoint:650,
                settings: {
                    item:2,
                    slideMove:1
                  }
            }
        ]
    });
	$('#vertical').lightSlider({
      gallery:true,
      item:1,
      vertical:true,
      verticalHeight:400,
      vThumbWidth:50,
      thumbItem:8,
      thumbMargin:4,
      slideMargin:0
    });

    var autoplaySlider = $('#autoplay').lightSlider({
        auto:true,
        loop:true,
        pauseOnHover: true,
        onBeforeSlide: function (el) {
            $('#current').text(el.getCurrentSlideCount());
        } 
    });

   $('#total').text(autoplaySlider.getTotalSlideCount());

});