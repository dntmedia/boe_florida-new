<?php
session_start();

# INTERRUPT FOR SECURITY TESTING
if ($_POST["ACTION"]=="Login" and $_POST["PWD"]=="engARK123") {
	$_SESSION["LOGGEDIN"]=1;
	}

if ($_SESSION["LOGGEDIN"]!=1) {
	echo "<form method=\"post\">";
	echo "<p><strong>Password Please</strong><br /><input type=\"text\" name=\"PWD\" /></p>";
	echo "<input type=\"hidden\" name=\"ACTION\" value=\"Login\" />";
	echo "<input type=\"submit\" value=\"Login\" />";
	echo "</form>";
	exit();
	}


require_once('../libraries/includes/config.php');
require_once('../libraries/includes/functions.php');
?>
<html>
<head>
<style type='text/css'>
	form {
	width: 100%;
	margin: 0 auto;
	}

	label, input {
		display: inline-block;
	}

	label {
		width: 30%;
		text-align: right;
	}

	label + input {
		width: 30%;
		margin: 0 30% 2% 4%;
	}

	input + input {
		float: right;
		margin: 0 33% 0 0;
	}

	.success {
		margin: 0 30% 2% 30%;
		padding: 2%;
		background: green;
	}
	.add-btn {
		margin: 2% 30% 2% 30%;
	}
</style>
</head>
<body>

<h2 class='content-heading'>Edit Local Branch</h2>

<?php

$action = isset($_GET['action']) ? $_GET['action'] : 'index';

if($_POST && $action == 'update'){
	updateOffice($_GET['id'], $_POST);
	echo '<div class="success"> Updated ' . $_POST['NAME'] . '<br />
		<a href="' . $_SERVER['PHP_SELF'] . '">Go back</a><br /></div>';
}

if ($_POST && $action == 'add') {
	insertOffice($_POST);
	echo '<div class="success"> Added ' . $_POST['NAME'] . '<br />
		<a href="' . $_SERVER['PHP_SELF'] . '">Go back</a><br /></div>';
}

if ($action == 'delete')
{
	deleteOffice($_GET['id']);
	echo '<div class="success"> Office deleted!<br />
		<a href="' . $_SERVER['PHP_SELF'] . '">Go back</a><br /></div>';
}

if ($action == 'add')
{ ?>
	<form action="<?php echo $_SERVER['PHP_SELF'] ?>?action=add" method="POST">
		<label for="BRANCH">Branch: </label>
		<input type="text" name="BRANCH" value="<?php echo $office['BRANCH'] ?>" />
		<label for="NAME">Name: </label>
		<input type="text" name="NAME" value="<?php echo $office['NAME'] ?>" />
		<label for="ADDRESS">Address: </label>
		<input type="text" name="ADDRESS" value="<?php echo $office['ADDRESS'] ?>" />
		<label for="CITY">City: </label>
		<input type="text" name="CITY" value="<?php echo $office['CITY'] ?>" />
		<label for="STATE">State: </label>
		<input type="text" name="STATE" value="<?php echo $office['STATE'] ?>" />
		<label for="ZIP">Zip: </label>
		<input type="text" name="ZIP" value="<?php echo $office['ZIP'] ?>" />
		<label for="MANAGER">Manager: </label>
		<input type="text" name="MANAGER" value="<?php echo $office['MANAGER'] ?>" />
		<label for="EMAIL">Email: </label>
		<input type="text" name="EMAIL" value="<?php echo $office['EMAIL'] ?>" />
		<label for="PHONE">Phone: </label>
		<input type="text" name="PHONE" value="<?php echo $office['PHONE'] ?>" />
		<label for="FAX">FAX: </label>
		<input type="text" name="FAX" value="<?php echo $office['FAX'] ?>" />
		<label for="URL">URL: </label>
		<input type="text" name="URL" value="<?php echo $office['URL'] ?>" />
		<label for="ACTIVE">Active: </label>
		<input type="checkbox" name="ACTIVE" value="Yes" <?php
				if ($office['ACTIVE'] == 'Yes') echo 'checked' ?> />
		<input type="submit" value="Save" />
	</form>

<?php }

if($action == 'update'){
	$office = getOffice($_GET['id']);
?>

	<form action="<?php echo $_SERVER['PHP_SELF'] ?>?action=update&id=<?php echo $_GET['id'] ?>" method="POST">
		<label for="BRANCH">Branch: </label>
		<input type="text" name="BRANCH" value="<?php echo $office['BRANCH'] ?>" />
		<label for="NAME">Name: </label>
		<input type="text" name="NAME" value="<?php echo $office['NAME'] ?>" />
		<label for="ADDRESS">Address: </label>
		<input type="text" name="ADDRESS" value="<?php echo $office['ADDRESS'] ?>" />
		<label for="CITY">City: </label>
		<input type="text" name="CITY" value="<?php echo $office['CITY'] ?>" />
		<label for="STATE">State: </label>
		<input type="text" name="STATE" value="<?php echo $office['STATE'] ?>" />
		<label for="ZIP">Zip: </label>
		<input type="text" name="ZIP" value="<?php echo $office['ZIP'] ?>" />
		<label for="MANAGER">Manager: </label>
		<input type="text" name="MANAGER" value="<?php echo $office['MANAGER'] ?>" />
		<label for="EMAIL">Email: </label>
		<input type="text" name="EMAIL" value="<?php echo $office['EMAIL'] ?>" />
		<label for="PHONE">Phone: </label>
		<input type="text" name="PHONE" value="<?php echo $office['PHONE'] ?>" />
		<label for="FAX">FAX: </label>
		<input type="text" name="FAX" value="<?php echo $office['FAX'] ?>" />
		<label for="URL">URL: </label>
		<input type="text" name="URL" value="<?php echo $office['URL'] ?>" />
		<label for="ACTIVE">Active: </label>
		<input type="checkbox" name="ACTIVE" value="Yes" <?php
				if ($office['ACTIVE'] == 'Yes') echo 'checked' ?> />

		<input type="submit" value="Save" />
	</form>

	<a href="?id=<?php echo $_GET['id'] ?>&action=delete">Delete</a>

<?php
}

if ($action == 'index') {
	$offices = getOffices();

?>


	<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="GET">
		<input type="hidden" name="action" value="update" />
		<label for="id">Branch: </label>
		<select name='id'>
		<?php foreach ($offices as $office) : ?>
		<option value='<?php echo $office['ID'] ?>'><?php echo $office['NAME'] ?></option>
		<?php endforeach; ?>
		</select>
		<input type='submit' value='Edit' class='submit'/>
	</form>


	<a href="<?php echo $_SERVER['PHP_SELF'] ?>?action=add" class="add-btn">Add New</a>


<?php } ?>
</body>
</html>